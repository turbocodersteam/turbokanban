import { TurboKanbanWebPage } from './app.po';

describe('turbo-kanban-web App', () => {
  let page: TurboKanbanWebPage;

  beforeEach(() => {
    page = new TurboKanbanWebPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
