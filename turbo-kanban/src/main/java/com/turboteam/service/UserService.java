package com.turboteam.service;

import com.turboteam.model.User;
import com.turboteam.model.security.UserRole;

import java.util.Set;

/**
 * Created by adam on 19.07.17.
 */
public interface UserService {
    User findByUsername(String username);

    User findByEmail(String email);

    boolean checkUserExists(String username, String email);

    boolean checkUsernameExists(String username);

    boolean checkEmailExists(String email);

    void save (User user);

    User createUser(User user, Set<UserRole> userRoles);
}
