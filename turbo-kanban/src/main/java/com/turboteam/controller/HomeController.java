package com.turboteam.controller;

import com.turboteam.dao.RoleDao;
import com.turboteam.model.User;
import com.turboteam.model.security.UserRole;
import com.turboteam.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.security.Principal;
import java.util.HashSet;
import java.util.Set;

@Controller
public class HomeController {

    private final UserService userService;
    private final RoleDao roleDao;

    @Autowired
    public HomeController(UserService userService, RoleDao roleDao) {
        this.userService = userService;
        this.roleDao = roleDao;
    }

    @Value("${url.signup}")
    private String signupAddress;

    @Value("${url.index}")
    private String indexAddress;

    @RequestMapping("/")
    public String home() {
        return "redirect:/" + indexAddress;
    }

    @RequestMapping("/index")
    public String index() {
        return indexAddress;
    }

    @RequestMapping(value = "/signup", method = RequestMethod.GET)
    public String signup() {
        return signupAddress;
    }

    @RequestMapping(value = "/signup", method = RequestMethod.POST)
    public String signupPost(@ModelAttribute("user") User user, Model model) {

        if(userService.checkUserExists(user.getUsername(), user.getEmail())) {

            if(userService.checkUsernameExists(user.getUsername())){
                model.addAttribute("usernameExists", true);
            }

            if(userService.checkEmailExists(user.getEmail())){
                model.addAttribute("emailExists", true);
            }

            return "signup";
        } else {
            Set<UserRole> userRoles = new HashSet<>();

            userRoles.add(new UserRole(user, roleDao.findByName("ROLE_USER")));

            userService.createUser(user, userRoles);

            return "redirect:/";
        }
    }

    @RequestMapping("/userFront")
    public String userFront(Principal principal, Model model) {
        return "userFront";
    }
}
