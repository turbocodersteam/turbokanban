package com.turboteam.model;

public enum  TaskStatus {
    TO_DO,
    IN_PROGRESS,
    DONE
}
