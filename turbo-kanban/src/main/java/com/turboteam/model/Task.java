package com.turboteam.model;

import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@Table(name = "kb_task")
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long taskId;
    private String taskName;
    private String description;

    @Enumerated(EnumType.STRING)
    private TaskStatus status;

    @OneToMany(mappedBy = "task")
    private Set<UserTask> userTasks;

    @ManyToOne
    @JoinColumn(name="user_id")
    private User assignee;

    @ManyToOne
    @JoinColumn(name="project_id")
    private Project project;
}
