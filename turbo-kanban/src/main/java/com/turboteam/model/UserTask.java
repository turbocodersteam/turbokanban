package com.turboteam.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "tb_user_task")
public class UserTask {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long usersTaskId;
    private Long loggedHour;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "task_id")
    private Task task;
}
