package com.turboteam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TurboKanbanApplication {

    public static void main(String[] args) {
        SpringApplication.run(TurboKanbanApplication.class, args);
    }
}
